import React from "react";
import logo from "./logo.svg";
import "./App.css";

const App = ({ children, classComponent, functionalComponent }) => {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<p>
					Edit <code>src/App.js</code> and save to reload. Welcome to our APP!
				</p>
				<a
					className="App-link"
					href="https://reactjs.org"
					target="_blank"
					rel="noopener noreferrer"
				>
					Learn React
				</a>
				{children !== undefined ? children : "No children in component"}
				{classComponent}
				{functionalComponent}
			</header>
		</div>
	);
};

export default App;
